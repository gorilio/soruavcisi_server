'use strict';

var joi = require('joi');
var boom = require('boom');
var user_leaderboard_service = require('../service/user_leaderboards');

exports.getLeaderboard = {
    handler: function (request, reply) {
        user_leaderboard_service.getLeaderboard(function (err, leaderboard) {
            if (err) return reply(boom.badImplementation(err));
            reply(leaderboard.rows);
        });
    }
};

exports.getUserInLeaderboard = {
    handler: function (request, reply) {
        var user_id = request.params.user_id;
        user_leaderboard_service.getUserInLeaderboard(user_id, function (err, leaderboard) {
            if (err) return reply(boom.badImplementation(err));
            reply(leaderboard.rows);
        });
    }
};

exports.getDailyPointsChampion = {
    handler: function (request, reply) {
        user_leaderboard_service.getDailyPointsChampion(function (err, leaderboard) {
            if (err) return reply(boom.badImplementation(err));
            reply(leaderboard.rows[0]);
        });
    }
};

exports.getDailyStreakChampion = {
    handler: function (request, reply) {
        user_leaderboard_service.getDailyStreakChampion(function (err, leaderboard) {
            if (err) return reply(boom.badImplementation(err));
            reply(leaderboard.rows[0]);
        });
    }
};

exports.getDailyLeaderboard = {
    handler: function (request, reply) {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();

        if(dd<10) {
            dd='0'+dd
        }

        if(mm<10) {
            mm='0'+mm
        }

        today = yyyy + '-' + mm + '-' + dd;
        user_leaderboard_service.getDailyLeaderboard(today, function (err, leaderboard) {
            if (err) return reply(boom.badImplementation(err));
            reply(leaderboard.rows);
        });
    }
};