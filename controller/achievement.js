'use strict';

var joi = require('joi');
var boom = require('boom');
var user_achievement_service = require('../service/user_achievements');

exports.getAll = {
    handler: function (request, reply) {
        user_achievement_service.getAll(function(err, achievements) {
            if (err) return reply(boom.badImplementation(err));
            reply(achievements.rows);
        });
    }
};

exports.getUserAchievements = {
    handler: function (request, reply) {
        var user_id = request.params.user_id;
        user_achievement_service.getUserAchievements(user_id, function (err, achievements) {
            if (err) return reply(boom.badImplementation(err));
            reply(achievements.rows);
        });
    }
};

exports.create = {
    handler: function (request, reply) {
        var user_id = request.params.user_id;
        var achievement = request.payload;
        user_achievement_service.create(user_id, achievement.achievement_id, achievement.is_qualified, function(err, achievement) {
            if (err) return reply(boom.badImplementation(err));
            reply(achievement).created(achievement);
        });
    }
};

//Oyuncunun toplamda kac soru cevapladigini ceker
exports.getTotalAnsweredQuestionCount = {
    handler: function (request, reply) {
        var user_id = request.params.user_id;
        user_achievement_service.getTotalAnsweredQuestionCount(user_id, function (err, questionCount) {
            if (err) return reply(boom.badImplementation(err));
            if(questionCount.rows.length != 0){
                reply(questionCount.rows[0]);
            } else {
                reply(questionCount);
            }
        });
    }
};

//Oyuncunun toplamda kac soruyu DOGRU cevapladigini ceker
exports.getTotalCorrectAnsweredQuestionCount = {
    handler: function (request, reply) {
        var user_id = request.params.user_id;
        user_achievement_service.getTotalCorrectAnsweredQuestionCount(user_id, function (err, questionCount) {
            if (err) return reply(boom.badImplementation(err));
            reply(questionCount.rows[0]);
        });
    }
};

//Oyuncunun hangi kategoride kac soru cozdugunu gruplar
exports.getUserSolvedQuestionCountByCategory = {
    handler: function (request, reply) {
        var user_id = request.params.user_id;
        user_achievement_service.getSolvedQuestionCountByCategory(user_id, function (err, questionCounts) {
            if (err) return reply(boom.badImplementation(err));
            reply(questionCounts.rows[0]);
        });
    }
};

//Oyuncunun max yaptigi streak sayisi
exports.getUserMaxStreak = {
    handler: function (request, reply) {
        var user_id = request.params.user_id;
        user_achievement_service.getMaxStreak(user_id, function (err, maxStreak) {
            if (err) return reply(boom.badImplementation(err));
            reply(maxStreak.rows[0]);
        });
    }
};

//Oyuncu farkli gunlerde oyunu ne kadar oynadi
exports.getGamePlayCount = {
    handler: function (request, reply) {
        var user_id = request.params.user_id;
        user_achievement_service.getGamePlayCount(user_id, function (err, user) {
            if (err) return reply(boom.badImplementation(err));
            reply(user.rows.length);
        });
    }
};

exports.isQualified = {
    handler: function (request, reply) {
        var user_id = request.params.user_id;
        var achievement_id = request.params.achievement_id;
        user_achievement_service.isQualified(user_id, achievement_id, function (err, achievement) {
            if (err) return reply(boom.badImplementation(err));
            reply(achievement.rows[0]);
        });
    }
};

exports.getLeaderboard = {
    handler: function (request, reply) {
        var achievement_id = request.params.achievement_id;
        user_achievement_service.getLeaderboard(achievement_id, function (err, achievement) {
            if (err) return reply(boom.badImplementation(err));
            reply(achievement.rows);
        });
    }
};