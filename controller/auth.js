'use strict';

var securityTools = require('../tools/security');
var userService = require('../service/user');
var request = require('request');
var logger = require('../tools/logger');

var buildTokenObject = function (userId, username) {
    return {
        'facebookId': userId,
        'displayName': username,
        'time': Date.now()
    };
};

var handleFacebookUser = function (facebook_id, next) {
    userService.getByFacebookId(facebook_id, function (err, result) {
        if (err) return next(err);
        var user = result.rows[0];
        next(null, user);
    });
};

var getFacebookUserInfo = function (facebookAccessToken, next) {
    var facebookGraphApiUrl = 'https://graph.facebook.com/me?access_token=' + facebookAccessToken;
    request(facebookGraphApiUrl, function (error, response, body) {
        if (error) return next(error);
        if (response.statusCode !== 200) return next(response.body);

        next(null, JSON.parse(body));
    });
};

var responseAccessToken = function (accessToken, userId) {
    return {
        'access-token': accessToken,
        'userId': userId
    };
};

exports.generateAccessTokenAfterFacebookLogin = {
    handler: function (request, reply) {

        var facebookAccessToken = request.payload.facebookAccessToken;

        getFacebookUserInfo(facebookAccessToken, function (error, facebookProfile) {
            if (error) {
                logger.error(error);
                return reply(error);
            }

            var userData = buildTokenObject(facebookProfile.id, facebookProfile.name);
            var accessToken = securityTools.encrpyt(JSON.stringify(userData));

            // TODO: id ile de?il de email ile mi aramal??
            handleFacebookUser(facebookProfile.id, function (error, user) {
                if (error) {
                    logger.error(error);
                    return reply(error);
                }

                if (user) {
                    userService.updateAccessToken(user.id, accessToken, function (err, result) {
                        if (err) logger.error('updateAccessToken error', err);
                    });

                    return reply(responseAccessToken(accessToken, user.id));

                } else {
                    var userObj = {
                        fullName: facebookProfile.name,
                        username: facebookProfile.name,
                        email: facebookProfile.email || facebookProfile.name,//TODO: Joi tarafinda da unique lik kontrolu yapacakmiyiz?
                        device_id: 0,
                        points: 0,
                        level: 1,
                        facebook_id: facebookProfile.id,
                        facebook_display_name: facebookProfile.name,
                        access_token: accessToken
                    };

                    userService.create(
                        userObj.username,
                        userObj.fullName,
                        userObj.email,
                        userObj.device_id,
                        userObj.facebook_id,
                        userObj.facebook_display_name,
                        userObj.level,
                        userObj.points,
                        userObj.access_token,
                        function (err, result) {
                            if (err) {
                                logger.error(err);
                                return reply(err);
                            } else {
                                var tempUser = result.rows[0];
                                return reply(responseAccessToken(accessToken, tempUser.id));
                            }
                        }
                    );
                }
            });
        });
    }
};

exports.facebookLogin = {
    auth: 'facebook',
    handler: function (request, reply) {
        if (!request.auth.isAuthenticated) {
            return reply('Authentication failed due to: ' + request.auth.error.message);
        }

        // TODO: Perform any account lookup or registration, setup local session,
        // and redirect to the application. The third-party credentials are
        // stored in request.auth.credentials. Any query parameters from
        // the initial request are passed back via request.auth.credentials.query.

        var facebookProfile = request.auth.credentials.profile;
        var userData = buildTokenObject(facebookProfile.id, facebookProfile.displayName);
        var accessToken = securityTools.encrpyt(JSON.stringify(userData));

        return reply(responseAccessToken(accessToken, userData.id));
    }
};


