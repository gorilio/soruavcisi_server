'use strict';

var joi = require('joi');
var boom = require('boom');
var category_service = require('../service/category');

exports.getAll = {
    handler: function (request, reply) {
        category_service.getAll(function(err, categories) {
            if (err) return reply(boom.badImplementation(err));
            reply(categories.rows);
        });
    }
};

exports.getSingle = {
    handler: function (request, reply) {
        var category_id = request.params.category_id;
        category_service.getSingle(category_id, function (err, category) {
            if (err) return reply(boom.internal(err));
            reply(category.rows[0]);
        });
    }
};

exports.create = {
    validate: {
        payload: {
            name : joi.string().required()
        }
    },
    handler: function (request, reply) {
        var tmp_category = request.payload;
        category_service.create(tmp_category.name, function(err, category) {
            if (err) return reply(boom.badImplementation(err));
            reply(tmp_category).created(category);
        });
    }
};

exports.update = {
    validate: {
        payload: {
            id : joi.number().required(),
            name : joi.string().required(),
            create_date : joi.date()
        }
    },
    handler: function (request, reply) {
        var tmp_category = request.payload;
        category_service.update(tmp_category.id, tmp_category.name, function(err, category) {
            if(err) return reply(boom.badImplementation(err));
            reply(category);
        });
    }
};