'use strict';

var joi = require('joi');
var boom = require('boom');
var tag_service = require('../service/tag');

exports.getAll = {
    handler: function (request, reply) {
        tag_service.getAll(function(err, tags) {
            if (err) return reply(boom.badImplementation(err));
            reply(tags.rows);
        });
    }
};

exports.getSingle = {
    handler: function (request, reply) {
        var tagId = request.params.tag_id;
        tag_service.getSingle(tagId, function (err, tag) {
            if (err) return reply(boom.internal(err));
            reply(tag.rows[0]);
        });
    }
};

exports.create = {
    validate: {
        payload: {
            name : joi.string().required()
        }
    },
    handler: function (request, reply) {
        var tmp_tag = request.payload;
        tag_service.create(tmp_tag.name, function(err, tag) {
            if (err) return reply(boom.badImplementation(err));
            reply(tmp_tag).created(tag);
        });
    }
};

exports.update = {
    validate: {
        payload: {
            id : joi.number().required(),
            name : joi.string().required(),
            create_date : joi.date()
        }
    },
    handler: function (request, reply) {
        var tmp_tag = request.payload;
        tag_service.update(tmp_tag.id, tmp_tag.name, function(err, tag) {
            if(err) return reply(boom.badImplementation(err));
            reply(tag);
        });
    }
};