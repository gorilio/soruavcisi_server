'use strict';

var joi = require('joi');
var boom = require('boom');
var async = require('async');
var user_service = require('../service/user');
var question_service = require('../service/question');
var user_question_service = require('../service/user_questions');
var user_category_service = require('../service/user_categories');
var user_tag_service = require('../service/user_tags');
var user_round_service = require('../service/user_rounds');
var securityTools = require('../tools/security');
var logger = require('../tools/logger');

exports.getAll = {
    handler: function (request, reply) {
        user_service.getAll(function (err, users) {
            if (err) return reply(boom.badImplementation(err));
            reply(users.rows);
        });
    }
};

exports.getSingle = {
    handler: function (request, reply) {
        var user_id = request.params.user_id;
        user_service.getSingle(user_id, function (err, user) {
            if (err) return reply(boom.badImplementation(err));
            reply(user.rows[0]);
        });
    }
};

exports.getByUsername = {
    handler: function (request, reply) {
        var username = request.params.username;
        user_service.getByUsername(username, function (err, user) {
            if (err) return reply(boom.badImplementation(err));
            reply(user.rows[0]);
        });
    }
};

exports.create = {
    validate: {
        payload: {
            device_id: joi.string()
        }
    },
    handler: function (request, reply) {
        var user = request.payload;
        var random_number = Math.floor(Math.random() * 999999) + 1;
        user.username = "avci" + random_number;
        user.email = "";
        user.fullName = "";
        user.points = 0;

        var userDataForToken = {
            'device_id': user.device_id,
            'username': user.username,
            'time': new Date().toString()
        };

        user.access_token = securityTools.encrpyt(JSON.stringify(userDataForToken));

        user_service.create(user.username, user.fullName, user.email, user.device_id, 0, "", user.points, user.access_token, function (err, user) {
            if (err) return reply(boom.badImplementation(err));
            reply(user.rows[0]);
        });
    }
};

exports.createFacebookUser = {
    validate: {
        payload: {
            fullName: joi.string(),
            birthDate: joi.date(),
            username: joi.string(),
            email: joi.string(),//TODO: joi tarafinda da unique lik kontrolu yapacakmiyiz?
            device_id: joi.string(),
            points: joi.number().default(0),
            facebook_id: joi.string(),
            facebook_display_name: joi.string(),
            access_token: joi.string()
        }
    },
    handler: function (request, reply) {
        user_service.create(username, fullName, email, device_id, facebook_id, facebook_display_name, points, access_token, function (err, result) {
            if (err) return reply(boom.badImplementation(err));
            reply(result);
        });
    }
};

exports.updateProfileInfo = {
  validate: {
    payload: {
      username: joi.string()
    }
  },
  handler: function (request, reply) {
    var userId = request.params.user_id;
    var username = request.payload.username;

    user_service.updateUsername(userId, username, function(err, result){
        if (err) return reply(boom.badImplementation(err));
        reply(result);
    });
  }
};

exports.updatePoints = {
    validate: {
        payload: {
            points: joi.number()
        }
    },
    handler: function (request, reply) {
        var user_id = request.params.user_id;
        var user = request.payload;
        user_service.updatePoints(user_id, user.points, function (err, result) {
            if (err) return reply(boom.badImplementation(err));
            reply(result);
        });
    }
};

exports.solveQuestion = {
    validate: {
        payload: {
            question_id: joi.number().required(),
            level: joi.number().required(),//sorunun level i
            points: joi.number().required(),
            answer_id: joi.number().required(),
            is_correct: joi.bool().required(),
            answer_seconds: joi.number(),
            categories: joi.array(),
            tags: joi.array()
        }
    },
    handler: function (request, reply) {
        var user_id = request.params.user_id;
        var solved_question = request.payload;

        var updateUserCategories = function (category, callback) {
            user_category_service.updatePoints(user_id, category.id, solved_question.points, solved_question.level, callback);
        };
        var updateUserTags = function (tag, callback) {
            user_tag_service.updatePoints(user_id, tag.id, solved_question.points, callback);
        };
        async.series({
                'user_questions': function (callback) {
                    user_question_service.create(user_id, solved_question.question_id, solved_question.answer_id, solved_question.answer_seconds, callback);
                },
                'question': function (callback) {
                    if (solved_question.is_correct) {
                        question_service.updateAnswerStats(solved_question.question_id, 1, 0, callback);
                    }
                    else {
                        question_service.updateAnswerStats(solved_question.question_id, 0, 1, callback);
                    }
                },
                'user_categories_points': function (callback) {
                    async.each(solved_question.categories, updateUserCategories, callback);
                },
                'user_tags_points': function (callback) {
                    async.each(solved_question.tags, updateUserTags, callback);
                },
                'user_points': function (callback) {
                    if (solved_question.is_correct) {
                        user_service.updatePoints(user_id, solved_question.points, callback);
                    }
                },
            },
            function (err) {
                if (err) return reply(boom.badImplementation(err));
                reply();
            });
    }
};

exports.solveQuestions = {
    validate: {
        payload: {
            Items: joi.array()
        }
    },
    handler: function (request, reply) {
        var user_id = request.params.user_id;
        var solved_questions = request.payload.Items;

        var insertUserQuestion = function (solved_question, callback) {
            user_question_service.create(user_id, solved_question.question_id, solved_question.answer_id, solved_question.answer_seconds, solved_question.is_correct, callback);
        };
        var updateUserCategories = function (solved_question, category, callback) {
            user_category_service.updatePoints(user_id, category.id, solved_question.points, solved_question.level, callback);
        };

        async.each(solved_questions, function(q, callback){
            insertUserQuestion(q, callback);
            async.each(q.categories, function(category, callback) {
                updateUserCategories(q, category, callback);
            });
        },
        function (err) {
            if (err) return reply(boom.badImplementation(err));
            reply();
        });
    }
};

exports.createRound = {
    validate: {
        payload: {
            seconds: joi.number(),
            points: joi.number(),
            max_streak: joi.number()
        }
    },
    handler: function (request, reply) {
        var user_id = request.params.user_id;
        var round = request.payload;
        user_round_service.create(user_id, round.seconds, round.points, round.max_streak, function (err, result) {
            if (err) return reply(boom.badImplementation(err));
            reply(result);
        });
    }
};

exports.remove = {
    handler: function (request, reply) {

    }
};

exports.getCategories = {
    handler: function (request, reply) {
        var user_id = request.params.user_id;
        user_category_service.getAll(user_id, function (err, result) {
            if (err) return reply(boom.badImplementation(err));
            reply(result.rows);
        });
    }
};

exports.getActiveCategories = {
    handler: function (request, reply) {
        var user_id = request.params.user_id;
        user_category_service.getAllActive(user_id, function (err, result) {
            if (err) return reply(boom.badImplementation(err));
            reply(result.rows);
        });
    }
};

exports.addAllCategories = {
    handler: function (request, reply) {
        var user_id = request.params.user_id;
        user_category_service.createAll(user_id, function (err, result) {
            if (err) return reply(boom.badImplementation(err));
            reply(result);
        });
    }
};

exports.addCategory = {
  handler: function (request, reply) {
    var user_id = request.params.user_id;
    var category_id = request.params.category_id;
    user_category_service.create(user_id, category_id, function (err, result) {
      if (err) return reply(boom.badImplementation(err));
      reply(result);
    });
  }
};

exports.updateCategories = {
  validate: {
    payload: {
      userId: joi.number().required(),
      categoryIds: joi.array()
    }
  },
  handler: function (request, reply) {
    var userId = request.payload.userId;
    var categoryIds = request.payload.categoryIds;

    // 1. oyuncunun tüm kategorilerini deaktif yap
    user_category_service.deactivateAll(userId, function (err) {
      if (err) {
        logger.error('An error has occurred while deactivating all user categories');
        return reply(boom.badImplementation(err));
      }

      // 2. seçilen kategoriler içinde dön
      async.each(
        categoryIds,
        function (categoryId, callback) {
          user_category_service.get(userId, categoryId, function (err, result) {
            if (err) {
              logger.error('An error has occurred while getting user categories');
              return callback(err);
            }

            var userCategory = result.rows[0];
            if (userCategory) {
              // 3. seçilen kategory var, aktif yap
              user_category_service.activate(userId, categoryId, function (err) {
                if (err) logger.error('An error has occurred while activating a user category');
                callback(err);
              });
            } else {
              // 4. seçilen kategory yok, oluştur
              user_category_service.create(userId, categoryId, function (err) {
                if (err) logger.error('An error has occurred while creating a user category');
                callback(err);
              });
            }
          });
        },
        function (err) {
          if (err) {
            logger.error('An error has occurred while updating user categories.');
            return reply(boom.badImplementation(err));
          }
          return reply('ok');
        }
      );
    });
  }
};

exports.getPlaceInLeaderboard = {
    handler: function (request, reply) {
        var user_id = request.params.user_id;
        user_service.getLeaderboards(function (err, users) {
            if (err) return reply(boom.badImplementation(err));
            var user = users.rows.filter(function ( obj ) {
                return obj.id === parseInt(user_id);
            })[0];
            reply(user);
        });
    }
};