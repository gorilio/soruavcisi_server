'use strict';

var joi = require('joi');
var boom = require('boom');
var async = require('async');
var fs = require('fs');
var question_service = require('../service/question');
var answer_service = require('../service/answer');
var category_service = require('../service/category');
var question_category_service = require('../service/question_categories');
var question_tag_service = require('../service/question_tags');
var user_question_service = require('../service/user_questions');
var user_category_service = require('../service/user_categories');
var logger = require('../tools/logger');
var uuidV4 = require('uuid/v4');
var path = require('path');

var generateImageUrl = function (fileName) {
    return fileName ? `static.soruavcisi.com/images/${fileName}` : null;
};

exports.getAll = {
    handler: function (request, reply) {
        question_service.getAll(function (err, response) {
            if (err) return reply(boom.badImplementation(err));
            var questions = response.rows.map(function (question) {
                question.imageUrl = generateImageUrl(question.image); // add 'imageUrl'
                delete question.image; // remove 'image'
                return question;
            });
            reply(questions);
            // reply(response.rows);
        });
    }
};

exports.getAllUnknownWithAnswersCategories = {
    handler: function (request, reply) {

        var userId = request.params.user_id;
        if (!userId) {
            return reply(boom.badRequest('user_id is required'));
        }

        // get all questions, answers, categories
        async.parallel({
            'questions': function(callback) {
                question_service.getAllUserUnknownWithCategoryIds(userId, callback);
            },
            'answers': answer_service.getAll,
            'categories': category_service.getAll
        },
            function (err, results) {
                if (err) {
                    logger.error('An error has occurred while getting questions, answers, categories.');
                    return reply(boom.badImplementation(err));
                }

                var questions = results.questions.rows;
                var answers = results.answers.rows;
                var categories = results.categories.rows;

                // merge answers and categories with questions
                async.each(
                    questions,
                    function (question, callback) {
                        // append imageUrl
                        question.imageUrl = generateImageUrl(question.image);
                        delete question.image;

                        // append answers
                        question.answers = answers.filter(answer => answer.question_id == question.id);

                        // append categories
                        question.categories = categories.filter(category => question.category_ids.indexOf(category.id) > -1);
                        delete question.category_ids;

                        callback();
                    },
                    function (err) {
                        if (err) {
                            logger.error('An error has occurred while merging answers, categories with questions.');
                            return reply(boom.badImplementation(err));
                        }
                        reply(questions);
                    }
                );
            });
    }
};

exports.getSingle = {
    handler: function (request, reply) {
        var question_id = request.params.question_id;
        question_service.getSingle(question_id, function (err, response) {
            if (err) return reply(boom.internal(err));
            var question = response.rows[0];
            //question.imageUrl = generateImageUrl(question.image); // add 'imageUrl'
            //delete question.image; // remove 'image'
            async.parallel({
                'answers': function (callback) {
                    answer_service.getQuestionAnswers(question_id, callback);
                },
                'categories': function (callback) {
                    question_category_service.getQuestionCategories(question_id, callback);
                },
                // 'tags': function (callback) {
                //     question_tag_service.getQuestionTags(question_id, callback);
                // }
            },
                function (err, results) {
                    if (err) return reply(boom.badImplementation(err));

                    question.answers = results.answers.rows;
                    // question.tags = results.tags.rows;
                    question.categories = results.categories.rows;

                    reply(question);
                });
        });
    }
};

exports.create = {
    validate: {
        payload: {
            question: joi.string().required(),
            answers: joi.array({
                answer: joi.string().required(),
                isCorrect: joi.bool().required()
            }),
            categories: joi.array(),
            // tags: joi.array(),
            points: joi.number(),
            level: joi.number(),
            image: joi.string(),
            author: joi.string(),
            active: joi.bool().required()
        }
    },
    handler: function (request, reply) {
        var temp_question = request.payload;
        question_service.create(temp_question.question, temp_question.points, temp_question.level, temp_question.image, temp_question.author, temp_question.active, function (err, response) {
            if (err) return reply(boom.badImplementation(err));
            var question_id = response.rows[0].id;

            var insertAnswer = function (temp_answer, next) {
                answer_service.create(question_id, temp_answer.answer, temp_answer.is_correct, next);
            };
            var insertCategory = function (temp_category, next) {
                question_category_service.create(question_id, temp_category, next);
            };
            var insertTag = function (temp_tag, next) {
                question_tag_service.create(question_id, temp_tag, next);
            };

            // https://github.com/caolan/async
            // https://github.com/FredKSchott/the-node-way/blob/master/03-error-first-example.js#
            async.parallel({
                'answer': function (callback) {
                    async.each(temp_question.answers, insertAnswer, callback);
                },
                'category': function (callback) {
                    async.each(temp_question.categories, insertCategory, callback);
                },
                // 'tag': function (callback) {
                //     async.each(temp_question.tags, insertTag, callback);
                // }
            },
                function (err) {
                    if (err) return reply(boom.badImplementation(err));
                    reply(temp_question).created(response);
                }
            );
        });
    }
};

exports.uploadImage = {
    payload: {
        output: 'stream',
        parse: true,
        allow: 'multipart/form-data'
    },
    handler: function (request, reply) {
        var data = request.payload;
        if (data.file) {
            // get uploaded filename
            var fileName = data.file.hapi.filename;

            // get uploaded file extension
            var fileExtension = fileName.split('.').pop();

            // generate new filename
            var newFileName = uuidV4() + '.' + fileExtension;

            // create static directory if not exist
            var pathStatic = path.join(__base, 'static');
            if (!fs.existsSync(pathStatic)) {
                fs.mkdirSync(pathStatic);
            }

            // create iamges directory if not exist
            var pathImages = path.join(pathStatic, 'images');
            if (!fs.existsSync(pathImages)) {
                fs.mkdirSync(pathImages);
            }

            // get file path
            var filePath = path.join(pathImages, newFileName);
            var file = fs.createWriteStream(filePath);

            file.on('error', function (err) {
                logger.error(err);
                reply(JSON.stringify(err)); // ?
            });
            data.file.pipe(file);
            data.file.on('end', function (err) {
                var ret = {
                    filename: newFileName,
                    headers: data.file.hapi.headers
                };
                reply(JSON.stringify(ret));
            });
        }
    }
};

//henuz goruntulenmemis tek soru doner
exports.getUserUnseenQuestion = {
    handler: function (request, reply) {
        // once tum adamin kategorilerini alicaz. sonra aralarindan birini random secicez.
        // ilgili kategorideki level a gore yeni soru getiricez.
        // her bir soru cevaplandiktan sonra gecici olarak aldigi leveli adamin kategorisel hesabina yazicaz.
        // adam oyunu kapatip geri geldiginde ilgili kategoride hangi levelda kaldiysa soru zorlugu oradan baslicak

        user_category_service.getRandomOne(request.params.user_id, function (err, user) {
            var user_category_level = user.rows[0].level;
            var category_id = user.rows[0].category_id;
            var round_level_low;
            var round_level_high; // Bu bariyer sorunun zorluk seviyeinin ust bariyeri
            var round_streak = parseInt(request.params.streak);

            if (round_streak > 2) {
                round_level_low = user_category_level;
                round_level_high = user_category_level + 5;
            } else if (round_streak > 3) {
                round_level_low = user_category_level;
                round_level_high = user_category_level + 10;
            } else if (round_streak > 5) {
                round_level_low = user_category_level;
                round_level_high = user_category_level + 20;
            } else if (round_streak < -1) {
                round_level_low = user_category_level - 5;
                round_level_high = user_category_level;
            } else if (round_streak < -3) {
                round_level_low = user_category_level - 10;
                round_level_high = user_category_level;
            } else if (round_streak < -5) {
                round_level_low = user_category_level - 20;
                round_level_high = user_category_level;
            } else {
                round_level_low = user_category_level - 5;
                round_level_high = user_category_level + 5;
            }

            (function loop() {
                user_question_service.getUserLikeCategoriesUnseenQuestion(request.params.user_id, category_id, round_level_low, round_level_high, function (err, questions) {
                    if (err) return reply(boom.badImplementation(err));
                    var question = questions.rows[0];
                    //TODO: Recursive fonksyonlar RAM yerler, kontrol et...
                    //TODO: Soru bulamassa devamli bir loopta kaliyor ve 504 e dusuyor.
                    if (question == undefined) {
                        if (round_level_low < 1 && round_level_high > 100) {
                            // TODO: Artik kategori degistirmek ve eski kategoriden soru sormamak lazim. Adamin tek kategorisi vardiysa ona gore bi hata mesaji donmek gerek
                            console.log("504 e dusme sebebi olabilirmi?");
                        } else {
                            round_level_low = round_level_low - 5;
                            round_level_high = round_level_high + 5;
                            loop();
                        }
                    } else {
                        question.imageUrl = generateImageUrl(question.image); // add 'imageUrl'
                        delete question.image; // remove 'image'
                        async.parallel({
                            'answers': function (callback) {
                                answer_service.getQuestionAnswers(question.id, callback);
                            },
                            'categories': function (callback) {
                                question_category_service.getQuestionCategories(question.id, callback);
                            },
                            // 'tags': function (callback) {
                            //     question_tag_service.getQuestionTags(question.id, callback);
                            // }
                        },
                            function (err, results) {
                                if (err) return reply(boom.badImplementation(err));

                                question.answers = results.answers.rows;
                                // question.tags = results.tags.rows;
                                question.categories = results.categories.rows;

                                reply(question);
                            });
                    }
                });//service end
            } ());
        });
    }
};

exports.update = {
    validate: {
        payload: {
            id: joi.number().required(),
            question: joi.string().required(),
            answers: joi.array({
                answer: joi.string().required(),
                isCorrect: joi.bool().required()
            }),
            categories: joi.array(),
            // tags: joi.array(),
            points: joi.number(),
            level: joi.number(),
            author: joi.string(),
            image: joi.string().allow(null),
            create_date: joi.date(),
            active: joi.bool().required()
        }
    },
    handler: function (request, reply) {
        var question_id = request.params.question_id;
        var payload_question = request.payload;

        /* Eger update edilmesi icin gonderilen level mevcut olanla ayni deilse demekki admin tarafindan sorunun leveli begenilmemis ve tekrar
         * degeri degistirilmistir. Eger ayni ise sistem kendi kendine zorluk seviyesini hesap etmeye devam eder. Dogru cevaplanma yuzdesini bulur
         * ve devamli gelistirir. Eger sorunun cevaplanma sayisi 0 ise, yani soru henuz ilk defa bir oyuncu tarafindan cevaplaniyorsa, sorunun
         * zorluk seviyesi 0 olarak hesaplanmasin diye de kontrol yapilir.

         if(question.level == request.payload.level && question.correctAnswerCount != 0){
         question.level = 100 - Math.round((question.correctAnswerCount / question.viewCount) * 100); //Terse cevirmek icin 100den cikariyoruz. Sorunun bilinirlik orani yerine seviyesi cikmis oluyor
         } else if(question.level == request.payload.level && question.level != -1) {// Burada -1 koymamizin sebebi oyunda sorunun ilk yuklendiginde bu metodun ilk kez tetiklenip sorunun levelini ayarladiktan sonra soruyu tekrar cekmeye gerek kalmadan bu fonksyonu dogru cevapta tekrar tetiklemek icin. Eger tekrar cekmeden ve -1 koymadan bunu cagirirsak ilk cagirmada veritabaninda degisen deger tekrar eski haline gelmekte.
         question.level = request.payload.level;
         }*/

        var updateAnswer = function (tempAnswer, callback) {
            answer_service.update(tempAnswer.id, tempAnswer.answer, tempAnswer.is_correct, callback);
        };

        var insertCategory = function (category_id, callback) {
            question_category_service.create(question_id, category_id, callback);
        };

        var insertTags = function (tag_id, callback) {
            question_tag_service.create(question_id, tag_id, callback);
        };

        async.series({
            'question': function (callback) {
                question_service.update(question_id, payload_question.question, payload_question.image, payload_question.level, payload_question.points, payload_question.author, payload_question.active, callback);
            },
            'answers': function (callback) {
                async.each(payload_question.answers, updateAnswer, callback);
            },
            'question_categories_deleted': function (callback) {
                question_category_service.delete(question_id, callback);
            },
            'question_tags_deleted': function (callback) {
                question_tag_service.delete(question_id, callback);
            },
            'categories': function (callback) {
                async.each(payload_question.categories, insertCategory, callback);
            },
            // 'tags': function (callback) {
            //     async.each(payload_question.tags, insertTags, callback);
            // }
        },
            function (err, results) {
                if (err) return reply(boom.badImplementation(err));
                reply();
            });
    }
};

exports.updateViewStats = {
    validate: {
        payload: {
            view_count: joi.number().required()
        }
    },
    handler: function (request, reply) {
        var question_id = request.params.question_id;
        var view_count = request.payload.view_count;
        question_service.updateViewStats(question_id, view_count, function (err, question) {
            if (err) return reply(boom.badImplementation(err));
            reply(question);
        });
    }
};
