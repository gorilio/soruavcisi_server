'use strict';

var boom = require('boom');
var question_service = require('../service/question');
var category_service = require('../service/category');
var tag_service = require('../service/tag');

exports.getQuestionStats = {
    handler: function (request, reply) {
        question_service.getLevelStats(function(err, stats) {
            if (err) return reply(boom.badImplementation(err));
            reply(stats.rows);
        });
    }
};

exports.getQuestionCategoryCount = {
    handler: function (request, reply) {
        question_service.getCategoryQuestionCount(function(err, stats) {
            if (err) return reply(boom.badImplementation(err));
            reply(stats.rows);
        });
    }
};

exports.getCategoryStats = {
    handler: function (request, reply) {
        category_service.getStats(function(err, stats) {
            if (err) return reply(boom.badImplementation(err));
            reply(stats.rows);
        });
    }
};

exports.getTagStats = {
    handler: function (request, reply) {
        tag_service.getStats(function(err, stats) {
            if (err) return reply(boom.badImplementation(err));
            reply(stats.rows);
        });
    }
};