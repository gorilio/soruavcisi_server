var auth = require('./controller/auth');
var user = require('./controller/user');
var question = require('./controller/question');
var category = require('./controller/category');
var tag = require('./controller/tag');
var dashboard = require('./controller/dashboard');
var achievement = require('./controller/achievement');
var leaderboard = require('./controller/leaderboard');
var health = require('./controller/health');

// API Server Endpoints
exports.endpoints = [
    {method: ['GET', 'POST'], path: '/login', config: auth.facebookLogin},
    {method: 'POST', path: '/getAccessToken', config: auth.generateAccessTokenAfterFacebookLogin},

    // User operations
    {method: 'POST', path: '/newUser', config: user.create},
    {method: 'GET', path: '/allUsers', config: user.getAll},
    {method: 'GET', path: '/user/{user_id}', config: user.getSingle},
    {method: 'GET', path: '/usernames/{username}', config: user.getByUsername},
    {method: 'POST', path: '/user/{user_id}', config: user.updateProfileInfo},
    {method: 'POST', path: '/user/points/{user_id}', config: user.updatePoints},
    //TODO: Unity nin kendi WWW kutuphanesi PUT metodunu desteklemiyor. Bu sebeple POST yapildi. PUT olmasi gerek. UniWEB die bir kutuphane var ancak 25$.
    {method: 'POST', path: '/userQuestions/{user_id}', config: user.solveQuestions},
    {method: 'POST', path: '/user/newRound/{user_id}', config: user.createRound},
    //{method: 'POST', path: '/user/newCategory/{user_id}', config: user.addAllCategories},
    {method: 'GET', path: '/user/categories/{user_id}', config: user.getActiveCategories},
    {method: 'GET', path: '/user/newCategories/{user_id}', config: user.addAllCategories},
    {method: 'GET', path: '/user/newCategory/{user_id}/{category_id}', config: user.addCategory},
    {method: 'POST', path: '/user/updateCategories', config: user.updateCategories},
    {method: 'DELETE', path: '/user/{user_id}', config: user.remove},

    // Dashboard operations
    {method: 'GET', path: '/dashboard/categoryStats', config: dashboard.getCategoryStats},
    {method: 'GET', path: '/dashboard/categoryQuestionLevelStats', config: dashboard.getQuestionCategoryCount},
    {method: 'GET', path: '/dashboard/tagStats', config: dashboard.getTagStats},
    {method: 'GET', path: '/dashboard/questionLevelStats', config: dashboard.getQuestionStats},

    // Question operations
    {method: 'GET', path: '/allQuestions', config: question.getAll},
    {method: 'GET', path: '/allQuestionsWithAnswersAndCategories/{user_id}', config: question.getAllUnknownWithAnswersCategories},
    {method: 'GET', path: '/singleQuestion/{question_id}', config: question.getSingle},
    {method: 'GET', path: '/userQuestions/{user_id}/{streak}', config: question.getUserUnseenQuestion},
    {method: 'POST', path: '/newQuestion', config: question.create},
    {method: 'POST', path: '/uploadImage', config: question.uploadImage},
    {method: 'POST', path: '/updateQuestion/{question_id}', config: question.update},
    {method: 'POST', path: '/updateQuestionViewStats/{question_id}', config: question.updateViewStats},

    // Category operations
    {method: 'GET', path: '/allCategories', config: category.getAll},
    {method: 'GET', path: '/singleCategory/{category_id}', config: category.getSingle},
    {method: 'POST', path: '/newCategory', config: category.create},
    {method: 'POST', path: '/updateCategory/{category_id}', config: category.update},

    // Tag operations
    {method: 'GET', path: '/allTags', config: tag.getAll},
    {method: 'GET', path: '/singleTag/{tag_id}', config: tag.getSingle},
    {method: 'POST', path: '/newTag', config: tag.create},
    {method: 'POST', path: '/updateTag/{tag_id}', config: tag.update},

    //Achievement Operations
    {method: 'GET', path: '/allAchievements', config: achievement.getAll},
    {method: 'POST', path: '/newAchievement/{user_id}', config: achievement.create},
    {method: 'GET', path: '/achievements/{user_id}', config: achievement.getUserAchievements},
    {method: 'GET', path: '/achievement/{user_id}/{achievement_id}', config: achievement.isQualified},
    {method: 'GET', path: '/achievement/userQuestionCountByCategory/{user_id}', config: achievement.getUserSolvedQuestionCountByCategory},
    {method: 'GET', path: '/achievement/userTotalAnsweredQuestionCount/{user_id}', config: achievement.getTotalAnsweredQuestionCount},
    {method: 'GET', path: '/achievement/userTotalCorrectAnsweredQuestionCount/{user_id}', config: achievement.getTotalCorrectAnsweredQuestionCount},
    {method: 'GET', path: '/achievement/userMaxStreak/{user_id}', config: achievement.getUserMaxStreak},

    //Leaderboards Operations
    {method: 'GET', path: '/leaderboard', config: leaderboard.getLeaderboard},
    {method: 'GET', path: '/leaderboard/{user_id}', config: leaderboard.getUserInLeaderboard},
    {method: 'GET', path: '/leaderboard/dailyLeaderboard', config: leaderboard.getDailyLeaderboard},
    {method: 'GET', path: '/leaderboard/dailyStreakChampion', config: leaderboard.getDailyStreakChampion},
    {method: 'GET', path: '/leaderboard/dailyPointsChampion', config: leaderboard.getDailyPointsChampion},

    // Status
    {method: 'GET', path: '/health', config: health.check}
];