POST    http://localhost:8090/bulkQuestion
{ "questions" : [{
 "question": "Mustafa Kemal'in Samsun'a cikisi?",
 "answers" : [{ "answer" : "1919", "isCorrect" : true }, { "answer" : "1918", "isCorrect" : false }, { "answer" : "1923", "isCorrect" : false }],
 "categories": ["562a443587f81d1020dc8c15"],
 "tags": ["562a48058a59a8f920f0d560"],
 "points": 3,
 "level": 1
},{
 "question": "Tarihte bilinen ilk Turk Devleti?",
 "answers" : [{ "answer" : "Uygurlar", "isCorrect" : false }, { "answer" : "Asya Hun Devleti", "isCorrect" : true }, {"answer" : "Gokturkler", "isCorrect" : false }],
 "categories": ["562a443587f81d1020dc8c15"],
 "tags": ["562e3696b01709b04069733a"],
 "points": 3,
 "level": 1
},{
 "question": "Franz Becknbauer'in lakabi",
 "answers" : [{ "answer" : "Der Kaiser", "isCorrect" : true }, { "answer" : "Zugmaschine", "isCorrect" : false }, { "answer" : "King", "isCorrect" : false }],
 "categories": ["562a9e34c98e0a922e9a4439"],
 "tags": ["562e3804b01709b04069733b"],
 "points": 3,
 "level": 1
}]}


// t�m sorular cevapland���nda:
db.users.update({ _id: ObjectId("562a69082e4e9e71215b3fad") },{ $set:{questions : []}})