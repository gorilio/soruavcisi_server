/**
 * Created by gkc on 10/07/16.
 */

var now = function () {
  return '[' + new Date().toLocaleString() + '] ';
};

var print = function (message, type) {
  if (process.env.NODE_ENV === 'production') return;
  if (type === 'log') return console.log('[DEBUG]' + now() + message);
  else if (type === 'error') return console.error('[ERROR]' + now() + message);
};

exports.log = function (message) {
  print(message, 'log');
};

exports.error = function (message) {
  print(message, 'error');
};