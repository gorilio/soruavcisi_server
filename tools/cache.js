/**
 * Created by gkc on 16/12/2016.
 */

var cache = {};

module.exports = {
  set: function (key, item) {
    cache[key] = item;
  },
  get: function (key) {
    return cache[key];
  }
};