/**
 * Created by gkc on 15.10.2015.
 */

var crypto = require('crypto');
var config = require('../config')(process.env.NODE_ENV);

exports.encrpyt = function (text) {
  var cipher = crypto.createCipher('aes-256-cbc', config.encrpytKey);
  var crypted = cipher.update(text, 'utf8', 'hex');
  crypted += cipher.final('hex');
  return crypted;
};

exports.decrpyt = function (text) {
  try {
    var decipher = crypto.createDecipher('aes-256-cbc', config.encrpytKey);
    var dec = decipher.update(text, 'hex', 'utf8');
    dec += decipher.final('utf8');
    return dec;
  } catch (error) {
    return null;
  }
};

