FROM node:latest

# Create app directory
RUN mkdir -p /opt/soruavcisi-server
WORKDIR /opt/soruavcisi-server

#ENV NODE_ENV $NODE_ENV
#ENV NODE_ENV $NODE_ENV
#ENV NODE_ENV $NODE_ENV

# Bundle app source
COPY config config
COPY controller controller
COPY service service
COPY tools tools
COPY routes.js .
COPY server.js .
COPY package.json .

# Install app dependencies
RUN npm install

EXPOSE 8888
CMD ["npm", "start"]
