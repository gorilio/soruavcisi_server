'use strict';

var encryptKey = '=N(GE0+ıhc2984sce!2';

var local = {
  name: 'local',
  server: {
    host: '0.0.0.0',
    port: 9091
  },
  database: {
    connString: 'postgres://postgres:postgres@soruavcisi.com/soruavcisi_dev'
  },
  encrpytKey: encryptKey
};

var development = {
  name: 'development',
  server: {
    host: '0.0.0.0',
    port: 9091
  },
  database: {
    connString: 'postgres://localhost/soruavcisi'
  },
  encrpytKey: encryptKey
};

var production = {
  name: 'production',
  server: {
    host: '0.0.0.0',
    port: 8081
  },
  database: {
    connString: `postgresql://soruavcisi:${encodeURIComponent('~CUuh>w~9?S{D2Ck')}@db-prod.soruavcisi.com/soruavcisi_prod`
  },
  encrpytKey: encryptKey
};

var test = {
  name: 'test',
  server: {
    host: '0.0.0.0',
    port: 8081
  },
  database: {
    connString: `postgresql://soruavcisi:${encodeURIComponent('~CUuh>w~9?S{D2Ck')}@db-prod.soruavcisi.com/soruavcisi_test`
  },
  encrpytKey: encryptKey
};

module.exports = function (env) {

  switch (env) {
    case 'local':
      return local;
    case 'development':
      return development;
    case 'production':
      return production;
    case 'test':
      return test;
    default:
      throw Error('Undefined environment variable NODE_ENV=' + env);
  }
};
