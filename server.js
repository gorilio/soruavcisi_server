// Initialize opbeat monitoring before anything else
var getOpbeatConfig = function() {
  if (process.env.NODE_ENV === 'production') {
    return {
      appId: '006beac086',
      organizationId: 'ead0a4a59f9e4d7b9147ce73cb53df1a',
      secretToken: 'db3dbc724edfced39009209e6cb8e4c472eb67dd'
    };
  } else {
    return {
      appId: 'c558ddf4f6',
      organizationId: 'ead0a4a59f9e4d7b9147ce73cb53df1a',
      secretToken: 'db3dbc724edfced39009209e6cb8e4c472eb67dd'
    }
  }
};

var opbeatConfig = getOpbeatConfig();

var opbeat = require('opbeat').start(opbeatConfig);

var Hapi = require('hapi');
var config = require('./config')(process.env.NODE_ENV);
var oauth = require('./config/oauth');
var Routes = require('./routes');
var bell = require('bell');
var securityTools = require('./tools/security');
var logger = require('./tools/logger');
var server = new Hapi.Server();
global.__base = __dirname + '/';

var onServerStart = function (err) {
  if (err) return logger.error(err);
  logger.log(`Server started at ${server.info.uri}`);
  logger.log(`Environment : ${process.env.NODE_ENV} ${config.name} configuration loaded`);
};
var requestAllowed = function (req) {
  var allowedPaths = [
    '/login',
    '/getAccessToken',
    '/newUser',
    '/health'
  ];

  var isPathAllowed = allowedPaths.indexOf(req.path) > -1;
  var isProduction = process.env.NODE_ENV === 'production';

  return isPathAllowed || !isProduction;
};
var validateUser = function (accessToken, next) {
  try {
    var userDataString = securityTools.decrpyt(accessToken);
    var userData = JSON.parse(userDataString);
    if (userData === null) {
      return next({message: 'You need to be authenticated', code: 401});
    }

    // TODO: get facebookId/token from userAccessToken compare it with dbUser ?
  } catch (error) {
    return next({message: 'You need to be authenticated', code: 401});
  }

  next(null, userData);
};
var tokenExpired = function (tokenTime) {

  // stackoverflow.com/questions/16767301/calculate-difference-between-2-timestamps-using-javascript
  var tokenTime = new Date(tokenTime);
  var currentTime = new Date();
  var diffTime = currentTime - tokenTime;
  var diffMins = Math.floor(diffTime / 1000 / 60);
  var diffHours = Math.floor(diffTime / 1000 / 60 / 60);
  var diffDays = Math.floor(diffTime / 1000 / 60 / 60 / 24);

  // request authentication if token is older than 1 days
  if (diffDays > 1) {
    logger.log(`Access token is ${diffDays} days, ${diffHours} hours and ${diffMins} minutes old.`);
    return true;
  }
  return false;
};
var stopServer = function () {
  logger.log('Stopping server...');
  server.stop(function () {
    logger.log('Server stopped on purpose!');
  });
};

server.connection({
  host: config.server.host,
  port: process.env.NODE_PORT || config.server.port,
  routes: {
    cors: false
  }
});

server.register(bell, function (error) {
  if (error) return logger.error(error);

  server.auth.strategy('facebook', 'bell', {
    provider: 'facebook',
    password: oauth.facebook.password,
    clientId: oauth.facebook.clientId,
    clientSecret: oauth.facebook.clientSecret,
    isSecure: false
  });
  server.route(Routes.endpoints);
  server.start(onServerStart);
});

// server.ext('onRequest', function (req, res) {
//   var requestMethod = req.method.toUpperCase();
//   var requestMessageTail = `-> [${requestMethod}] ${req.path}`;

//   if (requestAllowed(req)) {
//     logger.log(`anonymous user ${requestMessageTail}`);
//     return res.continue();
//   }

//   var accessToken = req.headers.authorization;

//   validateUser(accessToken, function (err, userData) {
//     if (err) {
//       return res(err.message).code(err.code);
//     }

//     logger.log(`user ${userData.displayName} ${requestMessageTail}`);
//     if (tokenExpired(userData.time)) {
//       var msg = 'Token expired';
//       logger.log(msg);
//       return res({message: msg}).code(401);
//     }

//     res.continue();
//   });
// });

module.exports = server;

