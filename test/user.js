/**
 * Created by gkc on 15/12/2016.
 */

// During the test the env variable is set to test
process.env.NODE_ENV = 'test';

var server = require('../server');
var cache = require('../tools/cache');
var chai = require('chai');
var should = chai.should();
var assert = chai.assert;
var chaiHttp = require('chai-http');
chai.use(chaiHttp);

describe('User operations', function () {
  var randomUserId;

  describe('GET /allUsers', function () {
    it('should GET all users', function (done) {
      server.inject({
        method: 'GET',
        url: '/allUsers'
      }, function (res) {
        var allUsers = res.result;
        var randomUser = allUsers[Math.floor(Math.random() * allUsers.length)];
        randomUserId = randomUser.id;
        cache.set('randomUserId', randomUserId);

        assert.equal(res.statusCode, 200);
        assert.isDefined(allUsers);
        assert.isArray(allUsers);
        assert.property(randomUser, 'id');
        assert.isNumber(randomUser.id);
        assert.property(randomUser, 'username');
        assert.isString(randomUser.username);
        assert.property(randomUser, 'fullname');
        assert.isString(randomUser.fullname);
        assert.property(randomUser, 'email');
        assert.isString(randomUser.email);
        assert.property(randomUser, 'points');
        assert.isNumber(randomUser.points);
        assert.property(randomUser, 'categories');
        assert.isArray(randomUser.categories);
        assert.isOk(randomUser.categories.length > 0);
        assert.isString(randomUser.categories[0]);

        done();
      });
    });
  });

  describe('GET /user/{user_id}', function () {
    it('should GET a user', function (done) {
      server.inject({
        method: 'GET',
        url: '/user/' + randomUserId
      }, function (res) {
        var user = res.result;

        assert.equal(res.statusCode, 200);
        assert.isDefined(user);
        assert.isObject(user);
        assert.property(user, 'id');
        assert.isNumber(user.id);
        assert.property(user, 'username');
        assert.isString(user.username);
        assert.property(user, 'fullname');
        assert.isString(user.fullname);
        assert.property(user, 'email');
        assert.isString(user.email);
        assert.property(user, 'facebook_id');
        assert.isString(user.facebook_id);
        assert.property(user, 'facebook_displayname');
        assert.isString(user.facebook_displayname);
        assert.property(user, 'points');
        assert.isNumber(user.points);
        assert.property(user, 'access_token');
        assert.isString(user.access_token);
        assert.property(user, 'device_id');
        assert.isString(user.device_id);
        //assert.property(user, 'ticket_count');
        // assert.isNumber(user.ticket_count);  // TODO: check property

        done();
      });
    });
  });

  describe('GET /user/categories/{user_id}', function () {
    it('should GET user categories', function (done) {
      server.inject({
        method: 'GET',
        url: '/user/categories/' + randomUserId
      }, function (res) {
        var alluserCategories = res.result;
        var randomUserCategory = alluserCategories[Math.floor(Math.random() * alluserCategories.length)];

        assert.equal(res.statusCode, 200);
        assert.isDefined(alluserCategories);
        assert.isArray(alluserCategories);
        assert.property(randomUserCategory, 'user_id');
        assert.isNumber(randomUserCategory.user_id);
        assert.property(randomUserCategory, 'category_id');
        assert.isNumber(randomUserCategory.category_id);
        assert.property(randomUserCategory, 'points');
        assert.isNumber(randomUserCategory.points);
        assert.property(randomUserCategory, 'level');
        assert.isNumber(randomUserCategory.level);
        assert.property(randomUserCategory, 'is_active');
        // assert.isBoolean(randomUserCategory.is_active);  // TODO: check property

        done();
      });
    });
  });

  describe('POST /newUser', function () {
    it('should CREATE a new user', function (done) {
      var randomNumber = Math.floor(Math.random() * 9999) + 1;
      var randomDeviceId = 'test_device_id_' + randomNumber;

      server.inject({
        method: 'POST',
        url: '/newUser',
        payload: {
          /*'username': 'avcı_test',
           'fullname': 'avcı test user',
           'email': 'avci@test.com',*/
          'device_id': randomDeviceId,
          /*'facebook_id': '',
           'facebook_displayname': '',
           'points': 0,
           'access_token': ''*/
        }
      }, function (res) {
        assert.equal(res.statusCode, 200);
        done();
      });
    });
  });
});