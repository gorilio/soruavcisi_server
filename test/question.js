/**
 * Created by gkc on 15/12/2016.
 */

// During the test the env variable is set to test
process.env.NODE_ENV = 'test';

var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server');
var should = chai.should();
var assert = chai.assert;
chai.use(chaiHttp);
var cache = require('../tools/cache');
require('./user');  // to run user tests first because we need a randomUserId in here

describe('Question operations', function () {

  describe('GET /allQuestions', function () {
    it('should GET all users', function (done) {
      server.inject({
        method: 'GET',
        url: '/allQuestions'
      }, function (res) {
        var allQuestions = res.result;
        var randomQuestion = allQuestions[Math.floor(Math.random() * allQuestions.length)];
        randomQuestionId = randomQuestion.id;

        assert.equal(res.statusCode, 200);
        assert.isDefined(allQuestions);
        assert.isArray(allQuestions);
        assert.property(randomQuestion, 'id');
        assert.isNumber(randomQuestion.id);
        assert.property(randomQuestion, 'question');
        assert.isString(randomQuestion.question);
        assert.property(randomQuestion, 'points');
        assert.isNumber(randomQuestion.points);
        assert.property(randomQuestion, 'author');
        assert.isString(randomQuestion.author);
        assert.property(randomQuestion, 'active');
        assert.isBoolean(randomQuestion.active);
        assert.property(randomQuestion, 'level');
        assert.isNumber(randomQuestion.level);
        assert.property(randomQuestion, 'create_date');
        // TODO: check date type
        assert.property(randomQuestion, 'categories');
        assert.isArray(randomQuestion.categories);
        assert.isOk(randomQuestion.categories.length > 0);
        assert.isString(randomQuestion.categories[0]);

        done();
      });
    });
  });

  describe('GET /singleQuestion/{question_id}', function () {
    it('should GET a question', function (done) {
      server.inject({
        method: 'GET',
        url: '/singleQuestion/' + randomQuestionId
      }, function (res) {
        var question = res.result;

        assert.equal(res.statusCode, 200);
        assert.isDefined(question);
        assert.isObject(question);
        assert.property(question, 'id');
        assert.isNumber(question.id);
        assert.property(question, 'question');
        assert.isString(question.question);
        assert.property(question, 'level');
        assert.isNumber(question.level);
        assert.property(question, 'author');
        assert.isString(question.author);
        assert.property(question, 'active');
        assert.isBoolean(question.active);
        assert.property(question, 'create_date');
        // TODO: check date type
        assert.property(question, 'answers');
        assert.isArray(question.answers);
        assert.ok(question.answers.length > 0);
        assert.property(question.answers[0], 'id');
        assert.isNumber(question.answers[0].id);
        assert.property(question.answers[0], 'answer');
        assert.isString(question.answers[0].answer);
        assert.isNumber(question.answers[0].question_id);
        assert.propertyVal(question.answers[0], 'question_id', randomQuestionId);
        assert.property(question.answers[0], 'is_correct');
        assert.isBoolean(question.answers[0].is_correct);
        // assert.property(question, 'tags');
        // assert.isArray(question.tags);
        // if (question.tags.length > 0) {
        //   assert.property(question.tags[0], 'id');
        //   assert.isNumber(question.tags[0].id);
        //   assert.property(question.tags[0], 'name');
        //   assert.isString(question.tags[0].name);
        //   assert.property(question.tags[0], 'create_date');
        //   // TODO: check date type
        //   assert.property(question.tags[0], 'question_id');
        //   assert.isNumber(question.tags[0].question_id);
        //   assert.propertyVal(question.tags[0], 'question_id', randomQuestionId);
        //   assert.property(question.tags[0], 'tag_id');
        //   assert.isNumber(question.tags[0].tag_id);
        // }
        assert.property(question, 'categories');
        assert.isArray(question.categories);
        assert.ok(question.categories.length > 0);
        assert.property(question.categories[0], 'id');
        assert.isNumber(question.categories[0].id);
        assert.property(question.categories[0], 'name');
        assert.isString(question.categories[0].name);
        assert.property(question.categories[0], 'create_date');
        assert.property(question.categories[0], 'question_id');
        assert.isNumber(question.categories[0].question_id);
        assert.propertyVal(question.categories[0], 'question_id', randomQuestionId);
        assert.property(question.categories[0], 'category_id');
        assert.isNumber(question.categories[0].category_id);

        done();
      });
    });
  });

  // describe('GET /userQuestions/{user_id}/{streak}', function () {
  //   this.timeout(10000);

  //   var randomUserId;

  //   before(function (done) {
  //     randomUserId = cache.get('randomUserId');
  //     if (!randomUserId) {
  //       console.log(randomUserId);
  //       throw new Error('undefined randomUserId');
  //     }
  //     done();
  //   });

  //   it('should GET a question', function (done) {
  //     server.inject({
  //       method: 'GET',
  //       url: '/userQuestions/' + randomUserId + '/1'
  //     }, function (res) {
  //       var userQuestions = res.result;
  //       assert.equal(res.statusCode, 200);
  //       assert.isDefined(userQuestions);
  //       assert.isObject(userQuestions);

  //       done();
  //     });
  //   });
  // });
});