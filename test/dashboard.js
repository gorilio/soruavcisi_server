/**
 * Created by gkc on 15/12/2016.
 */

// During the test the env variable is set to test
process.env.NODE_ENV = 'test';

var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server');
var should = chai.should();
var assert = chai.assert;
chai.use(chaiHttp);

describe('Dashboard operations', function () {
  describe('GET /dashboard/categoryStats', function () {
    it('should GET category stats', function (done) {
      server.inject({
        method: 'GET',
        url: '/dashboard/categoryStats'
      }, function (res) {
        var categoryStats = res.result;
        var randomCategoryStat = categoryStats[Math.floor(Math.random() * categoryStats.length)];

        assert.equal(res.statusCode, 200);
        assert.isDefined(categoryStats);
        assert.isArray(categoryStats);
        assert.property(randomCategoryStat, 'name');
        assert.property(randomCategoryStat, 'count');
        assert.isString(randomCategoryStat.name);
        assert.isString(randomCategoryStat.count);

        done();
      });
    });
  });

  // describe('GET /dashboard/tagStats', function () {
  //   it('should GET tag stats', function (done) {
  //     server.inject({
  //       method: 'GET',
  //       url: '/dashboard/tagStats'
  //     }, function (res) {
  //       var tagStats = res.result;
  //       var randomTagStat = tagStats[Math.floor(Math.random() * tagStats.length)];

  //       assert.equal(res.statusCode, 200);
  //       assert.isDefined(tagStats);
  //       assert.isArray(tagStats);
  //       assert.property(randomTagStat, 'name');
  //       assert.property(randomTagStat, 'count');
  //       assert.isString(randomTagStat.name);
  //       assert.isString(randomTagStat.count);

  //       done();
  //     });
  //   });
  // });

  describe('GET /dashboard/questionLevelStats', function () {
    it('should GET question level stats', function (done) {
      server.inject({
        method: 'GET',
        url: '/dashboard/questionLevelStats'
      }, function (res) {
        var questionLevelStats = res.result;
        var randomQuestionLevelStat = questionLevelStats[Math.floor(Math.random() * questionLevelStats.length)];

        assert.equal(res.statusCode, 200);
        assert.isDefined(questionLevelStats);
        assert.isArray(questionLevelStats);
        assert.property(randomQuestionLevelStat, 'points');
        assert.property(randomQuestionLevelStat, 'count');
        assert.isNumber(randomQuestionLevelStat.points);
        assert.isString(randomQuestionLevelStat.count);

        done();
      });
    });
  });
});