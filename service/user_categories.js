'use strict';

var db = require('./db');

exports.getAll = function (user_id, next) {
    db.query(
        'select * from user_categories where user_id = $1;',
        [
            user_id
        ],
        next
    );
};

exports.getAllActive = function (user_id, next) {
    db.query(
        'select * from user_categories where user_id = $1 and is_active = true;',
        [
            user_id
        ],
        next
    );
};

exports.get = function (user_id, category_id, next) {
    db.query(
        'select * from user_categories where user_id = $1 and category_id=$2;',
        [
            user_id,
            category_id
        ],
        next
    );
};

exports.getRandomOne = function (user_id, next) {
    db.query(
        'select * from user_categories where user_id = $1 order by random() limit 1;',
        [
            user_id
        ],
        next
    );
};

exports.create = function (user_id, category_id, next) {
    db.query(
        'insert into user_categories (user_id, category_id, points, level, is_active) values ($1, $2, 0, 30, true);', //default olarak oyuncunun iligili kategorideki leveli cok kolay a denk gelen 30
        [
            user_id,
            category_id
        ],
        next
    );
};

exports.delete = function (user_id, next) {
  db.query(
    'delete from user_categories where user_id = $1;',
    [
      user_id,
    ],
    next
  );
};

exports.createAll = function (user_id, next) {
    //oyuncunun kategori puanlarini default olarak 30 dan yani kolay seviyesinden baslatiyoruz.
    db.query(
        'insert into user_categories (select $1, id, 0, 30 from categories);',
        [
            user_id
        ],
        next
    );
};

exports.updatePoints = function (user_id, category_id, increment_points, level, next) {
    db.query(
        'update user_categories ' +
        'set points = points + $1, level = $4 ' +
        'where user_id = $2 and category_id = $3;',
        [
            increment_points,
            user_id,
            category_id,
            level
        ],
        next
    );
};

exports.deactivate = function(user_id, category_id, next){
    db.query(
        'update user_categories set is_active = false where user_id = $1 and category_id = $2;',
        [
            user_id,
            category_id
        ],
        next
    );
};

exports.activate = function(user_id, category_id, next){
    db.query(
        'update user_categories set is_active = true where user_id = $1 and category_id = $2;',
        [
            user_id,
            category_id
        ],
        next
    );
};

exports.deactivateAll = function (user_id, next) {
  db.query(
    'update user_categories set is_active = false where user_id = $1;',
    [
      user_id,
    ],
    next
  );
};