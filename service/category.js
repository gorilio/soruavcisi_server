'use strict';

var db = require('./db');

exports.getAll = function (next) {
    db.query(
        'select id, name from categories;',
        [],
        next
    );
};

exports.getSingle = function (category_id, next) {
    db.query(
        'select * from categories where id = $1;',
        [
            category_id
        ],
        next
    );
};

exports.getStats = function (next) {
    db.query(
        'select c.name, count(qc.category_id) as count ' +
        'from question_categories qc ' +
        'inner join categories c ' +
        'on c.id = qc.category_id ' +
        'group by c.name, qc.category_id',
        [],
        next
    );
};

exports.create = function (name, next) {
    db.query(
        'insert into categories (name, create_date) values ($1, now()::timestamp) returning id;',
        [
            name
        ],
        next
    );
};

exports.update = function (category_id, name, next) {
    db.query(
        'update categories set name = $1 where id = $2;',
        [
            name,
            category_id
        ],
        next
    );
};