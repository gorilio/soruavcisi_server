'use strict';

var db = require('./db');

exports.create = function (user_id, seconds, next) {
    db.query(
        'insert into rounds (user_id, seconds, create_date) values ($1, $2, now()::timestamp) returning id;',
        [
            user_id,
            seconds
        ],
        next
    );
};