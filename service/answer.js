'use strict';

var db = require('./db');

exports.getAll = function (next) {
    db.query(
        'select * from answers;',
        [],
        next
    );
};

exports.getQuestionAnswers = function (question_id, next) {
    db.query(
        'select * from answers where question_id = $1;',
        [
            question_id
        ],
        next
    );
};

exports.create = function (question_id, answer, is_correct, next) {
  db.query(
      'insert into answers (question_id, answer, is_correct) values ($1, $2, $3) returning id;',
      [
        question_id,
        answer,
        is_correct
      ],
      next
  );
};

exports.update = function (answer_id, answer, is_correct, next) {
    db.query(
        'update answers set answer = $1, is_correct = $2 where id = $3;',
        [
            answer,
            is_correct,
            answer_id
        ],
        next
    );
};