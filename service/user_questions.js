'use strict';

var db = require('./db');

exports.getUserUnseenQuestion = function (user_id, round_level_low, round_level_high, next) {
    db.query(
        'select q.id, q.question, q.level, q.points, q.view_count, q.image ' +
        'from questions q ' +
        'where q.level > $2 and q.level < $3 and q.active = true and ' +
        'q.id ' +
        'not in ( ' +
            'select uq.question_id ' +
            'from user_questions uq ' +
            'where uq.user_id = $1 ' +
        ') ' +
        'order by random() ' +
        'limit 1',
        [
            user_id,
            round_level_low,
            round_level_high
        ],
        next
    );
};

exports.getUserLikeCategoriesUnseenQuestion = function (user_id, category_id, round_level_low, round_level_high, next) {
    db.query(
        'select q.id, q.question, q.level, q.points, q.view_count, q.image ' +
        'from questions q inner join question_categories qc on q.id = qc.question_id ' +
        'where q.level > $3 and q.level < $4 and q.active = true and qc.category_id = $2 and q.id ' +
        'not in ( ' +
            'select uq.question_id ' +
            'from user_questions uq ' +
            'where uq.user_id = $1 ' +
        ') ' +
        'order by random() ' +
        'limit 1',
        [
            user_id,
            category_id,
            round_level_low,
            round_level_high
        ],
        next
    );
};

exports.create = function (user_id, question_id, answer_id, answer_seconds, is_correct_answer, next) {
    db.query(
        'insert into user_questions (user_id, question_id, answer_id, answer_seconds, is_correct_answer, create_date) values ($1, $2, $3, $4, $5, now()::timestamp);',
        [
            user_id,
            question_id,
            answer_id,
            answer_seconds,
            is_correct_answer
        ],
        next
    );
};