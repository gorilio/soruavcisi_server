'use strict';

var db = require('./db');

exports.getAll = function (next) {
    db.query(
        'select * from achievements;',
        [],
        next
    );
};

exports.getUserAchievements = function (user_id, next) {
    db.query(
        'select a.name, a.description, ua.is_qualified from user_achievements ua inner join achievements a on ua.achievement_id = a.id ' +
        'where ua.user_id = $1;',
        [
            user_id
        ],
        next
    );
};

exports.create = function (user_id, achievement_id, is_qualified, next) {
    db.query(
        'insert into user_achievements (user_id, achievement_id, is_qualified) values ($1, $2, $3);',
        [
            user_id,
            achievement_id,
            is_qualified
        ],
        next
    )
};

exports.getSolvedQuestionCountByCategory = function (user_id, next) {
    db.query(
        'select count(qc.question_id) as question_count, qc.category_id from user_questions uq ' +
        'inner join question_categories qc ' +
        'on qc.question_id = uq.question_id ' +
        'where uq.user_id = $1 ' +
        'group by qc.category_id;',
        [
            user_id
        ],
        next
    );
};

exports.getTotalAnsweredQuestionCount = function (user_id, next) {
    db.query(
        'select count(uq.question_id) as question_count from user_questions uq ' +
        'where uq.user_id = $1;',
        [
            user_id
        ],
        next
    );
};

exports.getTotalCorrectAnsweredQuestionCount = function (user_id, next) {
    db.query(
        'select count(uq.question_id) as question_count, a.is_correct ' +
        'from user_questions uq inner join answers a ' +
        'on uq.answer_id = a.id ' +
        'where uq.user_id = $1 and a.is_correct = true ' +
        'group by(a.is_correct);',
        [
            user_id
        ],
        next
    );
};

exports.getMaxStreak = function (user_id, next) {
    db.query(
        'select max_streak from user_rounds ' +
        'where user_id = $1 ' +
        'order by max_streak desc ' +
        'limit 1;',
        [
            user_id
        ],
        next
    );
};

exports.getGamePlayCount = function (user_id, next) {
    db.query(
        'select create_date from user_rounds ' +
        'where user_id = $1 ' +
        'group by create_date;',
        [
            user_id
        ],
        next
    );
};

exports.isQualified = function (user_id, achievement_id, next) {
    db.query(
        'select * from user_achievements ' +
        'where user_id = $1 and achievement_id = $2;',
        [
            user_id,
            achievement_id
        ],
        next
    );
};