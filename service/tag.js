'use strict';

var db = require('./db');

exports.getAll = function (next) {
    db.query(
        'select * from tags;',
        [],
        next
    );
};

exports.getSingle = function (tag_id, next) {
    db.query(
        'select * from tags where id = $1;',
        [
            tag_id
        ],
        next
    );
};

exports.getStats = function (next) {
    db.query(
        'select t.name, count(qt.tag_id) as count ' +
        'from question_tags qt ' +
        'inner join tags t ' +
        'on t.id = qt.tag_id ' +
        'group by t.name, qt.tag_id',
        [],
        next
    );
};

exports.create = function (name, next) {
    db.query(
        'insert into tags (name, create_date) values ($1, now()::timestamp) returning id;',
        [
            name
        ],
        next
    );
};

exports.update = function (tag_id, name, next) {
    db.query(
        'update tags set name = $1 where id = $2;',
        [
            name,
            tag_id
        ],
        next
    );
};