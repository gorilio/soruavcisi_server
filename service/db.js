var pg = require('pg');
var config = require('../config')(process.env.NODE_ENV);
var connstring = config.database.connString;

module.exports = {
    query: function (text, values, next) {
        pg.connect(connstring, function (error, client, done) {
            if (error) return next(error);
            client.query(text, values, function (error, result) {
                done();
                next(error, result);
            });
        });
    }
};
