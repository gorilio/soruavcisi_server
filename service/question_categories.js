'use strict';

var db = require('./db');

exports.create = function (question_id, category_id, next) {
    db.query(
        'insert into question_categories (question_id, category_id) values ($1, $2);',
        [
            question_id,
            category_id
        ],
        next
    );
};

exports.getQuestionCategories = function (question_id, next) {
    db.query(
        'select * ' +
        'from categories c ' +
        'inner join question_categories qc on qc.category_id = c.id ' +
        'where qc.question_id = $1;',
        [
            question_id
        ],
        next
    );
};

exports.delete = function (question_id, next) {
    db.query(
        'delete from question_categories where question_id = $1;',
        [
            question_id
        ],
        next
    );
};