'use strict';

var db = require('./db');

exports.getLeaderboard = function (next) {
    db.query(
        'select u.id, u.username, u.points from users u ' +
        'order by u.points desc;',
        [],
        next
    );
};

exports.getUserInLeaderboard = function (user_id, next) {
    db.query(
        'select u.username, u.points from users u ' +
        'where u.id = $1 ',
        [
            user_id
        ],
        next
    );
};

exports.getDailyStreakChampion = function (next) {
    db.query(
        'select * from user_rounds ur ' +
        'inner join users u ' +
        'on u.id = ur.user_id ' +
        'order by max_streak desc ' +
        'limit 1;',
        [],
        next
    );
};

exports.getDailyPointsChampion = function (next) {
    db.query(
        'select * from user_rounds ur ' +
        'inner join users u ' +
        'on u.id = ur.user_id ' +
        'order by ur.points desc ' +
        'limit 1;',
        [],
        next
    );
};

exports.getDailyLeaderboard = function (create_date, next) {
    db.query(
        'select user_id, (select username from users where id = user_rounds.user_id), sum(points) as points from user_rounds ' +
        'where create_date = $1 ' +
        'group by user_id ' +
        'order by points desc;',
        [
            create_date
        ],
        next
    );
};