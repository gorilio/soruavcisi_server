'use strict';

var db = require('./db');

exports.create = function (question_id, tag_id, next) {
    db.query(
        'insert into question_tags (question_id, tag_id) values ($1, $2);',
        [
            question_id,
            tag_id
        ],
        next
    );
};

exports.getQuestionTags = function (question_id, next) {
    db.query(
        'select *' +
        'from tags t ' +
        'inner join question_tags qt ' +
        'on qt.tag_id = t.id ' +
        'where qt.question_id = $1;',
        [
            question_id
        ],
        next
    );
};

exports.delete = function (question_id, next) {
    db.query(
        'delete from question_tags where question_id = $1;',
        [
            question_id
        ],
        next
    );
};