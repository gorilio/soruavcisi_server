'use strict';

var db = require('./db');

exports.getAll = function (next) {
    db.query(
        'select u.id, u.username, u.fullname, u.email, u.points, array_agg(c.name) as categories ' +
        'from users u ' +
        'inner join user_categories uc ' +
        'on u.id = uc.user_id ' +
        'inner join categories as c ' +
        'on c.id = uc.category_id ' +
        'group by(u.id);',
        [],
        next
    );
};

exports.getSingle = function (user_id, next) {
    db.query(
        'select * from users where id = $1;',
        [
            user_id
        ],
        next
    );
};

exports.getByUsername = function (username, next) {
    db.query(
        'select * from users where username = $1;',
        [
            username
        ],
        next
    );
};

exports.getByFacebookId = function (facebook_id, next) {
    db.query(
        'select * from users where facebook_id = $1;',
        [
            facebook_id
        ],
        next
    );
};

exports.create = function (username, fullName, email, device_id, facebook_id, facebook_displayname, points, access_token, next) {
    db.query(
        'insert into users (username, fullname, email, device_id, facebook_id, facebook_displayname, points, access_token) ' +
        'values ($1, $2, $3, $4, $5, $6, $7, $8) returning id, access_token;',
        [
            username,
            fullName,
            email,
            device_id,
            facebook_id,
            facebook_displayname,
            points,
            access_token
        ],
        next
    );
};

exports.updateAccessToken = function (user_id, access_token, next) {
    db.query(
        'update users set access_token = $2 where id = $1;',
        [
            user_id,
            access_token
        ],
        next
    );
};

exports.updatePoints = function (user_id, increase_points, next) {
    db.query(
        'update users ' +
        'set points = points + $1 ' +
        'where id = $2;',
        [
            increase_points,
            user_id
        ],
        next
    );
};

exports.updatePoints = function (user_id, points, next) {
    db.query(
        'update users ' +
        'set points = $2 ' +
        'where id = $1;',
        [
            user_id,
            points
        ],
        next
    );
};

exports.getLeaderboards = function (next) {
    db.query(
        'select row_number() over(order by points desc), id, points from users;',
        [],
        next
    );
};

exports.updateUsername = function (user_id, username, next) {
  db.query(
      'update users set username = $2 where id = $1;',
      [
        user_id,
        username
      ],
      next
  );
};