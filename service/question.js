'use strict';

var db = require('./db');

exports.getAll = function (next) {
    db.query(
        'select q.id, q.question, q.image, q.points, q.author, q.active, q.level, q.create_date, array_agg(c.name) as categories ' +
        'from questions as q ' +
        'inner join question_categories as qc ' +
        'on q.id = qc.question_id ' +
        'inner join categories as c ' +
        'on c.id = qc.category_id ' +
        'group by(q.id)',
        [],
        next
    );
};

// gkc - getAll 'daki dönen categories yerine category_ids döndüren sorgu, diğeriyle işimiz yoksa silinebilir, bu kalmalı.
exports.getAllWithCategoryIds = function (next) {
    db.query(
        'select     q.id, q.question, q.image, q.points, q.active, q.level, array_agg(c.id) as category_ids ' +
        'from       questions as q ' +
        'inner join question_categories as qc on q.id = qc.question_id ' +
        'inner join categories as c on c.id = qc.category_id ' +
        'where      q.active ' +
        'group by   q.id;',
        [],
        next
    );
};

exports.getAllUserUnknownWithCategoryIds = function (user_id, next) {
    db.query(
        'SELECT 		q.id, q.question, q.image, q.points, q.active, q.level, array_agg(c.id) AS category_ids ' +
        'FROM 		    questions q ' +
        'INNER JOIN 	question_categories qc ON qc.question_id = q.id ' +
        'INNER JOIN 	categories c ON c.id = qc.category_id ' +
        'WHERE 		    q.active  ' +
        'AND            q.id NOT IN ( ' +
        '                   SELECT 		uq.question_id  ' +
 		'		            FROM 		user_questions uq ' +
 		'                   INNER JOIN	answers a ON a.question_id = uq.question_id ' + // TODO: remove join by adding is_correct column to the user_questions table, and rename user_questions table -> user_answers
 		'                   WHERE 		uq.user_id=$1 ' +
        '                   AND         a.is_correct=true ' +
        '               ) ' +
        'GROUP BY       q.id;',
        [
            user_id
        ],
        next
    );
};

exports.getSingle = function (question_id, next) {
    db.query(
        'select q.id, q.question, q.points, q.image, q.level, q.author, q.active, q.create_date ' +//, array_agg(a.answer) as answers ' +
        'from questions q ' +
        'inner join answers a ' +
        'on a.question_id = q.id ' +
        'where q.id = $1',// +
        //'group by (q.id) ',
        [
            question_id
        ],
        next
    );
};

exports.getLevelStats = function (next) {
    db.query(
        'select q.points, count(q.points) ' +
        'from questions q ' +
        'group by (q.points) ' +
        'order by q.points',
        [],
        next
    );
};

exports.getCategoryQuestionCount = function (next) {
    db.query(
        'select c.name, q.points, count(q.id) from questions q ' +
        'inner join question_categories qc ' +
        'on q.id = qc.question_id ' +
        'inner join categories c ' +
        'on qc.category_id = c.id ' +
        'group by(q.points, c.name) ' +
        'order by c.name, q.points',
        [],
        next
    );
};

exports.getSingleAnswers = function (question_id, next) {
    db.query(
        'select id, answer, is_correct ' +
        'from answers where question_id = $1 ',
        [
            question_id
        ],
        next
    );
};

exports.create = function (question, points, level, image, author, active, next) {
    db.query(
        'insert into questions (question, points, level, image, author, create_date, active, view_count, correct_answer_count, false_answer_count) ' +
        'values($1, $2, $3, $4, $5, now()::timestamp, $6, 0, 0, 0) returning id;',
        [
            question,
            points,
            level,
            image,
            author,
            active
        ],
        next
    );
};

exports.update = function (question_id, question, image, level, points, author, active, next) {
    db.query(
        'update questions ' +
        'set question = $1, image = $2, level = $3, points = $4, author = $5, active = $6 where id = $7;',
        [
            question,
            image,
            level,
            points,
            author,
            active,
            question_id
        ],
        next
    );
};

exports.updateViewStats = function (question_id, view_count, next) {
    db.query(
      'update questions ' +
      'set view_count = $1 where id = $2;',
      [
          view_count,
          question_id
      ],
      next
    );
};

exports.updateAnswerStats = function (question_id, correct_answer_increment, false_answer_increment, next) {
    /*sistem kendi kendine zorluk seviyesini hesap etmeye devam eder. Dogru cevaplanma yuzdesini bulur
     * ve devamli gelistirir. Eger sorunun cevaplanma sayisi 0 ise, yani soru henuz ilk defa bir oyuncu tarafindan cevaplaniyorsa, sorunun
     * zorluk seviyesi 0 olarak hesaplanmasin diye de kontrol yapilir.*/

    db.query(
      'update questions ' +
      'set correct_answer_count = correct_answer_count + $1, false_answer_count = false_answer_count + $2, level = 100 - (correct_answer_count * 100 / view_count) ' +
      'where id = $3;',
      [
          correct_answer_increment,
          false_answer_increment,
          question_id
      ],
      next
    );
};