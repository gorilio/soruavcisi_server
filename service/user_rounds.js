'use strict';

var db = require('./db');

exports.create = function (user_id, seconds, points, max_streak, next) {
    db.query(
        'insert into user_rounds (user_id, seconds, create_date, points, max_streak) values ($1, $2, now()::timestamp, $3, $4);',
        [
            user_id,
            seconds,
            points,
            max_streak
        ],
        next
    );
};