'use strict';

var db = require('./db');

exports.create = function (user_id, tag_id, next) {
    db.query(
        'insert into user_tags (user_id, category_id, create_date) values ($1, $2, now()::timestamp);',
        [
            user_id,
            tag_id
        ],
        next
    );
};

exports.updatePoints = function (user_id, tag_id, increment_points, next) {
    db.query(
        'update user_tags ' +
        'set points = points + $1 ' +
        'where user_id = $2 and tag_id = $3;',
        [
            increment_points,
            user_id,
            tag_id
        ],
        next
    );
};